<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Graphical visualization of text similarities in essays in a book | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="en">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">

<div class="grossbild"><img src="02_originale/fritz_gross_460.jpg"></div>
<a href="sketches.php">...more example pictures</a>

<a name="Introduction"></a>
<h2>Introduction</h2>

<div class="textbild">
  <img src="px/totalinteraction.jpg" class="bild" style="width: 140px;">
</div>
<p>
In 2004 a book was published by the <a href="http://iad.hgkz.ch" target="_blank">Interaction Design Institute</a> of the University of Applied Sciences and Arts Zürich, on the subject of "the interaction" and its place in the world of man-machine-communications. 
 The book, Total Interaction, is a compilation of nineteen essays which offer different perspectives on this complex thematic field; the issues they tackle form the basis of the Institute’s work, research and teaching.
</p>
<p>
As <a href="http://iad.hgkz.ch/no_cache/de/projekte/projekt/total_interaction/" target="_blank">"Total Interaction"</a> took shape, it became apparent to its editors that the concepts under discussion would have to be illustrated, in order to help the reader understand not only the content of each individual essay, but also the thematic links relating the essays to each other. The authors thus undertook to create visually intuitive graphics, which, in representing the essays in a „second language“, would assist in the extraction and comprehension of more information from each essay (and the book) than reading alone would allow.
</p>

<a href="concepts.php">next page...</a>


<div style="clear: both;"></div>
<br /><br /><br /><br />

    </div>

<?php require( "_nav.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu en Navi
google_ad_channel = "5891472523";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="0229056246";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>

    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
