<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Grafische Visualisierung von Textähnlichkeiten zwischen Fach-Artikeln | munterbund.de</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Language" content="de">
	<meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
	<script src="../javascripts/prototype.js" type="text/javascript"></script>
	<script src="../javascripts/effects.js" type="text/javascript"></script>
	<script src="../javascripts/dragdrop.js" type="text/javascript"></script>
	<script src="../javascripts/controls.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

	<div id="container">

		<div id="center" class="column">
			
<a name="AKonzept"></a>
<h1 style="margin-top: 1em;">A Konzept</h1>

<a name="Problembeschreibung"></a>
<h2>Problembeschreibung</h2>

<p>
Eine Auswahl Artikel soll in visualisierenden Grafiken dargestellt werden. Die Grafiken sollen ein grobe thematische und strukturelle Einordnung der Artikel ermöglichen und Überblick über ihren Bezug zu anderen Artikeln der Auswahl geben.
</p>
<p>
Einige Artikel hängen thematisch eng zusammen, andere wiederum sind so speziell mit einem Thema befasst, dass sie beinahe alleine zu betrachten sind. Manche Artikel haben überraschende Querbezüge zu im ersten Moment nicht ähnlichen Artikeln, die aufgrund gemeinsamer Stichworte bestehen. Das Feld dieser Beziehungen sowohl ansprechend grafisch und typographisch, als auch gleichzeitig informativ darzustellen, war Herausforderung für die vorliegenden Inhaltsgrafiken.
</p>
<p>
Natürlich ist die gewählte Herangehensweise nur eine einzige unter vielen. Es gibt sicherlich einige Varianten, die ästhetischer ausgefallen wären, genauso, wie es solche geben wird, die mehr Informationsgehalt verpackt hätten. Wir wollten einen Mittelweg finden, der sich - auch wegen der Anzahl und des Umfangs der Artikel - automatisch durch definierte Regeln erzeugen lässt. Die grosse Menge langer Artikel in diesem Buch legt dies zudem nahe.
</p>
<a name="Visualisierungsgrundlage"></a>
<h2>Visualisierungsgrundlage</h2>

<p>
Eine Datengrafik, vergleichbar z.B. mit einem Balkendiagramm, visualisiert eine Menge an in gewissem Sinne quantifizierbaren Daten. Ein einfaches Beispiel wären Ausgaben eines Haushalts, die sich über den Zeitraum eines Jahres angesammelt haben. Dabei liesse sich in einem Balkendiagramm visualisieren, wie hoch die einzelnen Ausgaben waren und in welchen Zeiträumen sich eine gewisse Menge an Ausgaben gehäuft hat oder ausgeblieben ist. Dazu ist als Basis eine Reihe von Werten erforderlich, die in diesem Fall die Beträge der Ausgaben und deren Zeitpunkt angeben. Es lassen sich dann Platzierung und Höhe der Balken in der Grafik berechnen.
</p>
<div class="textbild">
	<img src="02_originale/perret_detail.jpg" class="bild">
	<div class="bildlegende">
		
			<a href="02_originale/perret_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
				Detailausschnitt Grafik zu Raphael Perret
	</div>
</div>
<p>
Sollen solche Inhaltsgrafiken automatisierbar erzeugt werden, bedingt dies auch die automatisierbare Verarbeitung der Information innerhalb eines Artikels zu quantifizierbaren Werten. Da zur Zeit Computersysteme noch nicht wirklich inhaltliche Intelligenz besitzen und Artikel auf eine, dem Menschen ähnliche Art und Weise "lesen" und verstehen können, waren wir bei der Visualisierung auf Basisdaten angewiesen, die quasi im Artikel selbst stecken und nicht durch "intelligentes Lesen" zum Artikel hinzugefügt, in einen Kontext gestellt, assoziiert oder interpretiert werden.
</p>
<p>
Eine wichtiger Aspekt war nun die Methode der Erhebung von Daten zu den jeweiligen Artikeln. Wir haben dabei aus einer Menge von Möglichkeiten schlussendlich die, vom Aufwand her betrachtet, praktikabelsten herausgelöst und in Grafiken umgewandelt. Mit einer zusätzlichen Auswahl aufwändiger zu erhebender Metadaten hätte die Grafik eventuell noch informativer gestaltet werden können. Nachfolgend ein kurzer Überblick über alle in Betracht gezogenen Daten.
</p>
<a name="AnsaetzederDatenauswahl"></a>
<h2>Ansätze der Datenauswahl</h2>

<p>
Die Menge an Daten, die zu einem Artikel erhoben werden könnten lässt sich in zwei Gruppen teilen. Die erste Gruppe besteht aus Daten, die quasi "manuell", also in unserem Fall durch menschliche Intelligenz, erhoben werden müssen. Die zweite Gruppe lässt sich automatisch durch maschinelle Intelligenz erheben.
</p>
<a name="Stichworte"></a>
<h3>Stichworte</h3>

<p>
Stichworte sind Worte, die den Artikel in besonderer Weise kennzeichnen oder die für den Artikel erhöhte Aussagekraft besitzen. Stichworte wären z.B. Begriffe bei denen der Artikel als Suchresultat aufgeführt würde, wenn jemand in diese Richtung recherchieren sollte. Stichworte können im Artikel selbst auftauchen oder hinzugefügt werden. In jedem Falle bedürfen sie aber einer besonderen Kennzeichnung durch z.B. den Autor, einen Rezensenten oder den Leser. Stichworte sind subjektiv, denn sie sind eine Auszeichnung aus der Perspektive des Rezipienten und dessen thematischen Umfeldes.
</p>
<a name="Metadaten"></a>
<h3>Metadaten</h3>

<p>
Mögliche Metadaten zu einem Artikel wären zum Beispiel Autor, Entstehungszeitraum, Sprache, Anzahl und Art der Bilder, Textgattung, adressiertes Publikum, Länge des Textes (in Zeichen, Worten, Sätzen, Abschnitten, Seiten), Dateiformat des Textes, verwendeter Zeichensatz und so weiter. Dabei stellen wir fest, dass einige dieser Metadaten automatisiert erhoben werden können. So lässt sich zum Beispiel die Sprache, in der der Artikel verfasst wurde, durch Vergleich der Worte des Artikels mit Worten bekannter Sprachen herausfinden. Enthält der Artikel hauptsächlich Worte, die beispielsweise dem rumänischen Sprachraum zugeordnet werden, wird er wohl in rumänisch verfasst sein<span id="zu_1">
	<a href="#" onclick="Element.toggle( 'zu_1', 'detail_1' ); return false;">[ad.01]</a>.
</span><span id="detail_1" class="fussnote" style="display: none;">
	<br />
	Anhand der Formulierung fällt auf, dass dies für Menschen absolut selbstverständlich ist. Ein Computer kann hier aber nur Zeichenmuster vergleichen, das Konstrukt einer "Sprache" ist ihm ohne aufwändige Zusatzprogrammierung gar nicht bekannt
	<a href="#" onclick="Element.toggle( 'zu_1', 'detail_1' ); return false;">[schliessen]</a>
	<br />
</span>Handelt es sich aber um Worte einer dem Computer nicht bekannten Sprache ist keine Aussage möglich.
</p>
<div class="textbild">
	<img src="03_fruehe_stadien/friedewald_11.jpg" class="bild">
	<div class="bildlegende">
		
			<a href="03_fruehe_stadien/friedewald_11_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
				Frühe Stadien der Visualisierung
	</div>
</div>
<p>
Der Entstehungszeitraum kann ebenso in manchen Fällen automatisiert verarbeitet werden, nämlich dann, wenn der Text auf einem Computer entstanden ist und das Textprogramm die Zeiträume der Bearbeitung mit protokolliert. Liegen die Bilder in digitaler Form vor, lassen sich auch darüber Metadaten automatisiert verarbeiten. Bilddateiformat, Dimensionen, Farbtiefe und Auflösung sind dann bekannt, Aussagen über den Bildinhalt wären aber manuell zu klassifizierende Metainformationen. Der Bereich der Metadaten muss also fallweise in manuell oder automatisch zu erhebend aufgeteilt werden. Im Gegensatz dazu ist die Länge des Textes zwar eine Metainformation, lässt sich aber so detailliert automatisch ermitteln, dass sie direkt in den Teil der automatisch zu ermittelnden Kriterien überleitet.
</p>
<a name="StatistischeDaten"></a>
<h3>Statistische Daten</h3>

<p>
Da ein Artikel üblicherweise in Form einer Sequenz von alphanumerischen Zeichen elektronisch gespeichert wird, ist es für den Computer ein Leichtes, diese Zeichen zu zählen. Statistische Auswertungen über die Anzahl Zeichen, Worte, Sätze, Abschnitte, Seiten oder deren Häufigkeiten (wobei in diesem Fall nur Zeichen und Worte zu brauchbaren Resultaten führen), sind einfach automatisch zu erfassen, haben aber als Datum nur eingeschränkte Aussagekraft. Interessanter werden diese Auswertungen im Vergleich mehrerer Artikel miteinander. Es lassen sich also sowohl Statistiken über einen Artikel erstellen, als auch Vergleiche zwischen zwei Artikeln, Vergleiche zwischen einem Artikel und der Menge der restlichen Artikel oder Auswertungen über den Umfang einer ganzen Artikelsammlung respektive eines Buches.
</p>
<a name="StrukturelleDaten"></a>
<h3>Strukturelle Daten</h3>

<p>
Mit Hilfe von computerlinguistischen Verfahren lässt sich auch grammatikalische und damit strukturelle Information aus einem Artikel automatisch ermitteln. Das bedingt aber, dass zum einen die Grammatik einer Sprache bekannt ist und verwendet wurde. Eine Sammlung von z.B. rein assoziativen Worten, die durchaus auch eine Aussage darstellen, lässt sich nicht linguistisch analysieren. Und selbst wenn die linguistische Analyse Ergebnisse erzielt, ist deren Aussagekraft über die Thematik des Artikels beschränkt. Die Resultate geben eher Aufschluss über Textgattung oder Komplexität der Ausdrucksweise des Autors<span id="zu_2">
	<a href="#" onclick="Element.toggle( 'zu_2', 'detail_2' ); return false;">[ad.02]</a>.
</span><span id="detail_2" class="fussnote" style="display: none;">
	<br />
	Und nur in manchen Fällen lässt dies Rückschlüsse auf die Komplexität des Themas zu. Oft gibt es Autoren, die selbst zu einfachen Sachverhalten komplexe Sätze schreiben. Bei Prosatexten kann es auch bewusst eingesetztes Stilmittel sein
	<a href="#" onclick="Element.toggle( 'zu_2', 'detail_2' ); return false;">[schliessen]</a>
	<br />
</span>
</p>
<a name="AuswahlimspeziellenFall"></a>
<h2>Auswahl im speziellen Fall</h2>

<div class="textbild">
	<img src="02_originale/buurman_detail.jpg" class="bild">
	<div class="bildlegende">
		
			<a href="02_originale/buurman_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
				Detailausschnitt Grafik zu Gerhard Buurman
	</div>
</div>
<p>
Im vorliegenden Fall haben wir uns auf die einfach automatisiert zu ermittelnden Daten gestützt. Als Datenbasis dient uns die Worthäufigkeit sowohl innerhalb eines Artikels, als auch im Vergleich mit anderen Artikeln. Darauf aufbauend haben wir die Visualisierungen generiert. Um detaillierte Grafiken mit möglichst thematischer Aussagekraft zu bekommen, haben wir die Metadaten ausser Acht gelassen und lediglich die Worthäufigkeiten betrachtet. Die nicht thematischen Füllwörter eines Artikels (wie z.B. es, und, man, wird, das) werden im Vorfeld aus der Statistik entfernt mittels einer manuell zusammengestellten, so genannten Stoppwortliste , die diese Wort ausfiltert.
</p>
<p>
Der Begriff des "Zweitautors" ist für unsere spezielle Wahl der Darstellung ein weiteres wichtiges Kriterium. Der Zweitautor wird pro untersuchtem Wort eines fraglichen Artikels ermittelt und gibt denjenigen Autor an, der in allen restlichen Artikeln dieses spezielle Wort am häufigsten verwendet hat. Damit stellt ein untersuchtes Wort einen thematischen Zusammenhang zwischen zwei Artikeln zumindest in statistischer Hinsicht her. Und die häufigste Verbindung dient uns als Visualisierungsinformation.
</p>

<a href="produktion.php">nächste Seite...</a>
<br/><br/><br/><br/>

		</div>

<?php require( "_nav_de.html" ); ?>

		<div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu de Navi
google_ad_channel = "0551351158";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="3544438418";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="7350177742";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="7350177742";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
		</div>

	</div>

	<div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
