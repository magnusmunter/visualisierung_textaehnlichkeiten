<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Grafische Visualisierung von Textähnlichkeiten zwischen Fach-Artikeln | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="de">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">
      
<a name="CBildbeispiele"></a>
<h1 style="margin-top: 1em;">C Bildbeispiele</h1>

<a name="FrueheStadienderVisualisierung"></a>
<h2>Frühe Stadien der Visualisierung</h2>

<p>
Die Entwicklung der Visualisierung durchschritt einen Prozess, der durchaus auch Sackgassen und Nebenzweige hervorbrachte. Hier können sie einige frühe Stadien betrachten.
</p>

<span class="beispielbild">
  <a href="03_fruehe_stadien/ackermann_01_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/ackermann_01.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/gisler_01_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/gisler_01.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/buurman_02_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/buurman_02.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/kersten_04_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/kersten_04.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/ackermann_06_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/ackermann_06.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/hug_07_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/hug_07.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/spaeth_10_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/spaeth_10.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/friedewald_11_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/friedewald_11.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/schubiger_12_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/schubiger_12.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/halter_13_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/halter_13.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/bisig_14_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/bisig_14.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/fjeld_15_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/fjeld_15.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/schmitz_16_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/schmitz_16.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/gisler_19_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/gisler_19.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="03_fruehe_stadien/weber_20_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="03_fruehe_stadien/weber_20.jpg" class="bild" /></a>
</span>

<div style="clear: both;"></div>
<br />
<a name="BeispieleEndergebnis"></a>
<h2>Beispiele Endergebnis</h2>

<p>
Hier einige Exemplare der am Schluss verwendeten Visualisierung in Gesamtansicht oder in einer detailierteren Vergrösserungsstufe..
</p>

<div>
<span class="beispielbild">
  <a href="02_originale/buurman_gross_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/buurman_gross.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/buurman_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/buurman_detail.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/fritz_gross_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/fritz_gross.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/fritz_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/fritz_detail.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/halter_gross_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/halter_gross.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/hug_gross_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/hug_gross.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/perret_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/perret_detail.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/schubiger_gross_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/schubiger_gross.jpg" class="bild" /></a>
</span>

<span class="beispielbild">
  <a href="02_originale/walz_gross_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="02_originale/walz_gross.jpg" class="bild" /></a>
</span>
</div>

<div style="clear: both;"></div>
<br /><br /><br /><br />

    </div>

<?php require( "_nav_de.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu de Navi
google_ad_channel = "0551351158";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="3544438418";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
