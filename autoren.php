<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Grafische Visualisierung von Textähnlichkeiten zwischen Fach-Artikeln | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="de">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">
      
<a name="DieAutoren"></a>
<h2 style="margin-top: 1em;">Die Autoren</h2>

<p>
<b>Magnus Rembold</b> ist Informatiker und Spezialist für Interaction Design, dynamische Informationsgrafiken, Datenbanken und Web-Applikationen. Er lehrt seit 2002 an der Hochschule für Gestaltung und Kunst in Zürich im Bereich Programmierung und Informationstechnologien.<br/>
Er entwickelte und beschrieb das vorliegende Konzept, programmierte die Erzeugung der Visualisierungen und die Umsetzung für die Einbindung in das Buch "Total Interaction".
</p>
<p>
<b>Jürgen Späth</b> ist Designer und Informationsspezialist. Er ist Professor für Informationsvisualisierung an der Hochschule für Gestaltung und Kunst in Zürich.<br />
Er entwarf die grafische Gestaltung für das Buch "Total Interaction" und legte den Grundstein zur Entstehung der Visualisierungen und begleitete das Projekt als grafischer Ratgeber und Antriebsmotor auf der Suche nach der optimalen Verbindung von Algorithmus und Ästhetik.<br />
Weitere Informationen: <a href="http://www.projekttriangle.com/about/about_people_jspaeth.htm">www.projekttriangle.com</a></p>

<a href="beispiele.php">nächste Seite...</a>
<br /><br /><br /><br />

    </div>

<?php require( "_nav_de.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu de Navi
google_ad_channel = "0551351158";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="3544438418";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
