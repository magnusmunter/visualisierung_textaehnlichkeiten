<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Grafische Visualisierung von Textähnlichkeiten zwischen Fach-Artikeln | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="de">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">
      
<a name="BProduktion"></a>
<h1 style="margin-top: 1em;">B Produktion</h1>

<a name="Implementierungskriterien"></a>
<h2>Implementierungskriterien</h2>

<p>
Die erzeugten Grafiken sollen im Buch in einer hohen Druckqualität dargestellt werden. Da sie nach unseren Vorstellungen hauptsächlich aus Typographie und Farbflächen bestehen sollen, fiel die Wahl auf ein Beschreibungsformat, welches Grafiken vektoriell und nicht pixelbasiert beschreibt. Zudem sollte das Format der Ausgabedatei durch einen Algorithmus geschrieben werden können, um einen voll automatisierbaren Prozess zu erhalten. Dazu war es wichtig, ein "offenes" Format zu wählen, dessen interne Syntax dokumentiert ist und das von den Programmen für das Layout des Buches<span id="zu_3">
  <a href="#" onclick="Element.toggle( 'zu_3', 'detail_3' ); return false;">[ad.03]</a>
</span><span id="detail_3" class="fussnote" style="display: none;">
  <br />
  Im konkreten Fall verwendeten wir Adobe Illustrator für Grafiken und Adobe InDesign für das Buch-Layout
  <a href="#" onclick="Element.toggle( 'zu_3', 'detail_3' ); return false;">[schliessen]</a>
  <br />
</span>problemlos weiterverarbeitet werden kann. Das<span id="zu_4">
  <a href="#" onclick="Element.toggle( 'zu_4', 'detail_4' ); return false;">Scalable Vector Graphics Format</a>
</span><span id="detail_4" class="fussnote" style="display: none;">
  <br />
  <b>Scalable Vector Graphics Format:</b> SVG, skalierbares Vektorgrafikformat, weitere Informationen unter <a href="http://www.w3.org/Graphics/SVG">http://www.w3.org/Graphics/SVG</a>
  <a href="#" onclick="Element.toggle( 'zu_4', 'detail_4' ); return false;">[schliessen]</a>
  <br />
</span>des<span id="zu_5">
  <a href="#" onclick="Element.toggle( 'zu_5', 'detail_5' ); return false;">W3C Gremiums</a>,
</span><span id="detail_5" class="fussnote" style="display: none;">
  <br />
  <b>World Wide Web Consortium:</b> W3C, eine Gruppe von Fachleuten, die sich um Standardisierung von elektronischen Datenformaten im Internet bemüht. Weitere Informationen unter <a href="http://www.w3.org">http://www.w3.org</a>
  <a href="#" onclick="Element.toggle( 'zu_5', 'detail_5' ); return false;">[schliessen]</a>
  <br />
</span>welches auf Vorschlag unter anderem von Adobe definiert wurde, erfüllte diese Anforderungen und brachte zudem den Vorteil mit sich, in einer<span id="zu_6">
  <a href="#" onclick="Element.toggle( 'zu_6', 'detail_6' ); return false;">XML-ähnlichen Form</a>
</span><span id="detail_6" class="fussnote" style="display: none;">
  <br />
  <b>XML:</b> eXtendable Markup Language -> erweiterbare Auszeichnungssprache. Eine Metasprache zur Formulierung von Datenformaten
  <a href="#" onclick="Element.toggle( 'zu_6', 'detail_6' ); return false;">[schliessen]</a>
  <br />
</span>auch für Menschen lesbar zu sein.
</p>
<p>
Schnell und flexibel Änderungen an den Regeln zur Erzeugung der Grafiken vornehmen zu können, war eine weitere Anforderung an den Generierungsprozess. Aus diesem Grund wurde mit Macromedia Director ein Autorensystem mit interpretierter Programmiersprache<span id="zu_7">
  <a href="#" onclick="Element.toggle( 'zu_7', 'detail_7' ); return false;">[ad.07]</a>
</span><span id="detail_7" class="fussnote" style="display: none;">
  <br />
  Director verwendet Lingo als interpretierte Programmiersprache. Deren Syntax ist stark an die beinahe natürlich-sprachliche und recht einleuchtende Sprache PASCAL von Niklaus Wirth von der ETH Zürich angelehnt
  <a href="#" onclick="Element.toggle( 'zu_7', 'detail_7' ); return false;">[schliessen]</a>
  <br />
</span>zur Implementierung des Algorithmus gewählt. Director ermöglicht direkten Eingriff in die Skriptverarbeitung ohne Neukompilierung und verfügt über mächtige Funktionen zur Manipulation von Texten, Worten und Zeichen. Weiterhin eröffnet das Autorensystem die Möglichkeit, neben der Ausgabe der Grafiken im SVG-Format diese ebenso ohne grösseren Aufwand innerhalb des Programms anzuzeigen.
</p>
<a name="Generierungsprozess"></a>
<h2>Generierungsprozess</h2>

<p>
Der Prozess der Generierung der Grafiken läuft in verschiedenen Schritten sequenziell ab und wird für jede neue Erzeugung derselben Grafik teilweise wiederholt. Er beginnt mit dem Einlesen und "Säubern" des Artikeltextes. Dabei werden Leerabsätze, Leerzeichen, Sonderzeichen und so weiter umgewandelt oder gestrichen. Der saubere Text wird dann Wort für Wort gefiltert (siehe "Stoppwortliste" weiter oben) und gezählt. Das Resultat ist eine Tabelle mit allen vorkommenden Worten des Artikels und deren Häufigkeit des Auftretens. Diese Datenaufbereitung muss nur einmal pro Artikel erfolgen und kann dann als Zwischenergebnis gespeichert werden. Daraus lässt sich beispielsweise berechnen werden, wieviele Worte Füllfunktion haben oder mehrfach vorkommen, wieviele Worte alleinstellend sind und diese Werte können auf die gesamte Länge des Artikels normiert werden.
</p>
<div class="textbild">
  <img src="03_fruehe_stadien/bisig_14.jpg" class="bild">
  <div class="bildlegende">
    
      <a href="03_fruehe_stadien/bisig_14_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
        Frühe Stadien der Visualisierung
  </div>
</div>
<p>
In einem zweiten Schritt werden Korrelationen zwischen Artikeln berechnet. Für jedes Wort wird der Zweitautor ermittelt und die relativen Häufigkeiten verglichen, um festzustellen, für welchen der beiden Autoren und ihre Artikel dieses Wort "wichtiger" ist.
</p>
<p>
Darauf folgen geometrische Berechnungen, die die erhobenen Daten in grafische Gebilde (in unserem Fall hauptsächlich Kreise) umwandeln. Dabei variieren die Position des Mittelpunkts, Radius, Füllfarbe, Textbeschriftungsfarbe und Transparenz der Kreise je nach erhobenen Daten.
</p>
<p>
Die grafischen Konstrukte werden in der Beschreibungssprache SVG ausgedrückt und in eine Ausgabedatei geschrieben. Nachdem alle Beschreibungen für eine Inhaltsgrafik zusammengefügt sind, wird die Datei geschlossen und an ein Browser-Programm mit Softwarebestandteil zur Anzeige von SVG-Dateien<span id="zu_8">
  <a href="#" onclick="Element.toggle( 'zu_8', 'detail_8' ); return false;">[ad.08]</a>
</span><span id="detail_8" class="fussnote" style="display: none;">
  <br />
  In unserem Fall der Browser Safari von Apple mit dem SVG-PlugIn von Adobe. Dies bekommt man unter <a href="http://www.adobe.com/svg/">http://www.adobe.com/svg/</a>
  <a href="#" onclick="Element.toggle( 'zu_8', 'detail_8' ); return false;">[schliessen]</a>
  <br />
</span>weitergeleitet. Dieser Bestandteil wandelt dann die Beschreibung in eine visuelle Form um.
</p>
<p>
Das verwendete Autorensystem Director besitzt selbst auch eine Anzeigemöglichkeit für vektorbasierte Grafiken. Damit wäre eine direkte Anzeige innerhalb des Autorensystems ebenso möglich und würde eventuell durch einfachere Eingriffsmöglichkeit während der Generierung den forschenden Entwicklungsprozess vereinfachen. In unserem Fall haben wir auf diese alternative Ausgabe aus Zeitgründen verzichtet.
</p>
<a name="InhaltlicheEbenenderGrafik"></a>
<h2>Inhaltliche Ebenen der Grafik</h2>

<p>
Die Inhaltsgrafiken lassen sich in drei strukturelle Ebenen aufgliedern, die jeweils unterschiedliche Informationen visualisieren.
</p>
<a name="Hintergrund"></a>
<h3>Hintergrund</h3>

<div class="textbild">
  <img src="01_grafiken/konzentr_kreise.jpg" class="bild">
  <div class="bildlegende">
        Radius der konzentrischen Kreise in Relation zur Wortanzahl der Artikel
  </div>
</div>
<p>
Der Hintergrund der Grafik befasst sich mit einer Einordnung des fraglichen Artikels in den Gesamtkontext der anderen Artikel.
</p>
<p>
Konzentrische Kreise repräsentieren alle Artikel des Buches, wobei der Radius proportional zur Anzahl der Zeichen eines Artikels ist. Die langen Artikel haben also die am weitesten aussen liegenden Kreise.
</p>
<div class="textbild">
  <img src="01_grafiken/kreis_sektoren.jpg" class="bild">
  <div class="bildlegende">
        Winkel und Radius der farbigen Kreissektoren im Verhältnis zu Anzahl und Übereinstimmung von Worten im Artikel
  </div>
</div>
<p>
Weiterhin gibt es einen nach dem Farbkreis nach <a href="http://de.wikipedia.org/wiki/Johannes_Itten" target="_blank">Johannes Itten</a> codierten Sektorabschnitt für jeden Artikel, der seinen Kreiswinkel aus der Anzahl der Worte bezieht, die zwischen dem fraglichen Artikel und dem Zweitautor übereinstimmen. Hat der Artikel mit einem weiteren Artikel viele Worte gemeinsam, wird der weitere Artikel mit einem grossen Farbsektor repräsentiert.
</p>
<p>
Der Radius des Sektorabschnitts ergibt sich wie bei den konzentrischen Ringen aus der Länge des jeweiligen Artikels. Die Artikel sind in der Abfolge innerhalb des Buches angeordnet, sodass der erste Artikel oben am Kreis startet und die weiteren im Uhrzeigersinn folgen.
</p>
<p>
Der Radius des weissen Freiraumes in der Mitte der Sektorabschnitte ist im Radius relativ zur Länge der anderen Artikel. Dabei ist die Distanz zwischen Mittelpunkt und Radius des Weissraums und die Distanz zwischen Weissraumradius und äusserstem konzentrischen Kreis proportional zum Verhältnis der Längen zwischen fraglichem Artikel und dem längsten Artikel des Buches. Ein kurzer Artikel hat also wenig zentralen Weissraum, ein langer Artikel hat davon viel. Die Segmentabschnitte haben dann "Längen" die der jeweiligen Artikellänge entsprechen. Zur Verdeutlichung des Farbcodes haben wir einen Aussenrand mit voll deckender Farbe verwendet, der am konzentrischen Kreis für diesen Artikel angesetzt ist.
</p>
<a name="Barcode-Struktur"></a>
<h3>Barcode-Struktur</h3>

<div class="textbild">
  <img src="01_grafiken/barcode.jpg" class="bild">
  <div class="bildlegende">
        Winkel und Farbe der Barcode-Struktur im Verhältnis zum Artikelaufbau
  </div>
</div>
<p>
Die Barcode-ähnliche Struktur, die innerhalb des konzentrischen Kreises des fraglichen Artikels angesetzt ist, schlüsselt die Struktur des Artikels selbst auf.
</p>
<p>
Der Kreiswinkel eines jeweiligen Abschnitts entspricht der Länge der Absätze untereinander, ebenso die Transparenz. Ein langer Abschnitt innerhalb des Textes hat eine helle Farbe (hohe Transparenz, niedrige Deckkraft) und einen grossen Kreiswinkel. Der Anteil der weissen Fläche am Ende der Barcodes hin zu einem Vollkreis ist proportional zur Länge der Fussnoten und Quellenhinweise am Ende des Textes<span id="zu_9">
  <a href="#" onclick="Element.toggle( 'zu_9', 'detail_9' ); return false;">[ad.09]</a>.
</span><span id="detail_9" class="fussnote" style="display: none;">
  <br />
  Dies ist das einzige manuell ausgezeichnete Metadatum in der Grafik
  <a href="#" onclick="Element.toggle( 'zu_9', 'detail_9' ); return false;">[schliessen]</a>
  <br />
</span>Ein grosser Weissbereich im Barcode-Ring deutet demnach auf einen Artikel mit vielen Fussnoten oder Quellenangaben hin.
</p>
<a name="Wortkreise"></a>
<h3>Wortkreise</h3>

<p>
In der Vordergrundebene liegen nun die Wortkreise, die eine Auswahl der häufigst vorkommenden Worte in einem Artikel wiederspiegeln. Die Häufigkeit (und damit teilweise auch die Wichtigkeit) eines Wortes ist proportional zu seinem Schriftgrad und zum Radius des Kreises. Die Farbe der Wortkreise entspricht der dem Autor zugeordneten Grundfarbe, die ihm durch den Farbkreis und die Stellung innerhalb des Buches zugeordnet ist. Sie wird mit einer reduzierten Deckkraft verwendet, um bei Überlagerungen verschiedener Wortkreise zu Verstärkungen und Hervorhebungen zu führen.
</p>
<div class="textbild">
  <img src="01_grafiken/wort_kreise.jpg" class="bild">
  <div class="bildlegende">
        Radius und Positionierung der Wortkreise zeigen die Wichtigkeit der Worte für visualisierten Artikel und ähnliche Artikel
  </div>
</div>
<p>
Die Position der Wortkreise zeigt zum einen an, welcher Zweitautor ein Wort ausserhalb des fraglichen Artikels am häufigsten verwendet hat. Entsprechend werden die Wortkreise in den jeweiligen Sektorenabschnitten plaziert. Zum anderen entspricht die Position auf der radialen Achse zwischen fraglichem Autor im Zentrum und Zweitautor am Rand dem Häufigkeitsverhältnis zwischen den beiden Artikeln. Ist der Begriff für den Zweitautor in dessen Artikel "wichtiger" als für den fraglichen Artikel, so ist der Wortkreis näher am Zweitautor plaziert und umgekehrt.
</p>
<p>
Wortkreise im Zentrum sind alleinstellend und haben keinen Zweitautor. Entsprechend ist ihre Schriftfarbe grau und ihre radiale Position im Weissraum entspricht ihrer Häufigkeit im Text. Worte im Zentrum sind dabei häufig, Worte am Rand selten. Der Kreiswinkel der Wortkreise wird entsprechend der alphabetischen Position ihres Anfangsbuchstabens berechnet. Innerhalb eines Sektorabschnitts sind im Uhrzeigersinn also zuerst Worte mit a, b, c, etc. platziert.
</p>

<a name="Zusammenfassung"></a>
<h2>Zusammenfassung</h2>

<p>
Mit einem möglichst voll automatisierbaren Verfahren, welches hauptsächlich auf Worthäufigkeiten basiert, sollte eine Inhaltsgrafik zu Artikeln geschaffen werden. Ein wichtiger Augenmerk lag dabei neben dem Informationsgehalt auf der (subjektiven) Ästhetik der Darstellung. Im beschriebenen Entwicklungsprozess achteten wir immer wieder auf den subjektiven Eindruck des Rezipienten und justierten die Regeln der Umsetzung von erhobenen Daten zu grafischen Konstrukten, um Leser über die Informationsgrafiken auch intuitiv zu inhaltlichen Schlüssen über den Artikel zu leiten. Ob dieser Versuch geglückt ist, entscheiden die Leserinnen und Leser...
</p>

<a href="autoren.php">nächste Seite...</a>
<br/><br/><br/><br/>

    </div>

<?php require( "_nav_de.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu de Navi
google_ad_channel = "0551351158";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="3544438418";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="7350177742";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="7350177742";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
