<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Graphical visualization of text similarities in essays in a book | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="en">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">

<a name="Abouttheauthors"></a>
<h2>About the authors</h2>

<p>
<b>Magnus Rembold</b> is a computer scientist and specialist in dynamic data graphing, databases and web-applications. Since 2002 he has taught in programming and information technologies, at the University of Applied Sciences and Arts in Zürich, Switzerland. He developed and described the concept outlined above, and programmed the creation of the visuals and their transformation for inclusion in the book "Total Interaction".
</p>
<p>
<b>Jürgen Späth</b> is a designer and information specialist. He is a professer at University of Applied Sciences and Arts in Zürich, Switzerland, and teaches basics of graphic and interaction design and information visualization. He designed the book "Total Interaction" and laid the foundation for the visuals. He accompanied the project as consultant and motivator in the search for an optimal connection between algorithm and aesthetics.<br />
Further information: <a href="http://www.projekttriangle.com/about/about_people_jspaeth.htm">www.projekttriangle.com</a>
</p>
<p>
This essay was translated partly by Adam Blauhut and Spring Gombe-Götz.
</p>

<a href="sketches.php">next page...</a>

<br /><br /><br /><br />
<br /><br /><br /><br />
<br /><br /><br /><br />
<br /><br /><br /><br />
<br /><br /><br /><br />
<br /><br /><br /><br />
<br /><br /><br /><br />
<br /><br /><br /><br />

    </div>

<?php require( "_nav.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu en Navi
google_ad_channel = "5891472523";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="0229056246";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
