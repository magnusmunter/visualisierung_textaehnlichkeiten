<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Graphical visualization of text similarities in essays in a book | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="en">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">

<a name="AConcepts"></a>
<h1 style="margin-top: 1em;">A Concepts</h1>

<a name="Theproblem"></a>
<h2>The problem</h2>

<p>
A collection of essays is collated for readers with visualizing graphics. The graphics should both serve as a thematic and structural overview of each text, and pose the essay in question in relation to the other essays in the book. They should be both an abbreviation of the text and the key to decoding the complex issues under discussion.
</p>
<p>
The difficulty in developing appropriate graphics arises from the level of discussion of the key themes. In some cases the relation between essays is apparent, a few of the essays have close thematic links. However, others focus so strongly on one topic that they more or less stand alone. In yet other cases, common keywords allow for surprising cross-references between two seemingly unrelated essays. The challenge is to find forms of graphical and/or typographical representation of the essays that are both appealing and informative. Care must be taken that the reader is not overwhelmed by either the number or the scope of the graphics. At the same time, each graphic must sufficiently summarise the text, and, in the interest of cohesion, each must be generated according to the same basic principles. Taking all of these issues into account, we have attempted create a system which automatically generates graphics according to predefined rules.
</p>

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
/* 468x15, Erstellt 31.03.09, Variante 3 */
google_ad_slot = "4418230863";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

<a name="Thebasisofvisualization"></a>
<h2>The basis of visualization</h2>

<p>
<div class="textbild">
  <img src="02_originale/perret_detail.jpg" class="bild">
  <div class="bildlegende">
    
      <a href="02_originale/perret_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
        Detail view of graphic for Raphael Perret
  </div>
</div>
A data graphic – like a bar chart – depicts quantifiable data. Any automated generation of content graphics requires an automated, quantifiable transformation of the (mostly textual) information in the essays. Unlike human beings, our current computing systems can only insufficiently "read" or interpret essays, which means that, in our work, we have had to rely exclusively on that data presented in each of the text, making no allowances for data that might be derived from interpretative reading or association.
</p>
<a name="Differentwaysofdataselection"></a>
<h2>Different ways of data selection</h2>

<p>
A significant constraint in developing appropriate graphics arises from the manner of data collection. In what follows, we would like to provide a brief overview of all the types of data under consideration.
</p> 
<p>
It is possible to divide data extracted from essays into two main groups: data that must be collected ”manually” (in our case, using human intelligence), and data that can be captured automatically by machine intelligence.
</p>
<a name="Keywords"></a>
<h3>Keywords</h3>

<p>
Keywords are terms that identify an essay in a special way or that have increased significance for it. If a person researches a subject using one of these keywords, it will produce a list of search results that should include the essay to which it is attached. Keywords may exist in or be added to an essay. In any case, they represent a special marker set by an author or cataloguer manually. Keywords are subjective – they reflect the specific perspective and background of the person who declares them.
</p>
<a name="Metadata"></a>
<h3>Metadata</h3>

<p>
Examples of essay metadata are the essay’s author, the time period in which an essay was written, the essay language, the number or type of images it uses, the text genre, the intended audience, the essay length (in characters, words, sentences, sections, pages), file format, typeface, etc. We find that it is possible to automate the collection of some of these metadata. For example, the language in which an article is written can be determined by comparing the words used in the article with words of other known languages. If the article contains words associated with the region where Romanian is spoken, it was most likely written in Romanian<span id="zu_1">
  <a href="#" onclick="Element.toggle( 'zu_1', 'detail_1' ); return false;">[ad.01]</a>.
</span><span id="detail_1" class="fussnote" style="display: none;">
  <br />
  The formulation alone makes clear that this is second nature for human beings. A computer can only compare character patterns; without extensive programming, it is unable to grasp the construct of language.
  <a href="#" onclick="Element.toggle( 'zu_1', 'detail_1' ); return false;">[schliessen]</a>
  <br />
</span>If words are unknown to the computer, it will not be able to determine the language.
</p>
<div class="textbild">
  <img src="03_fruehe_stadien/friedewald_11.jpg" class="bild">
  <div class="bildlegende">
    
      <a href="03_fruehe_stadien/friedewald_11_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
        Early stages in the process to the final result
  </div>
</div>
<p>
For the most part, the time period during which an essay was written can be retrieved automatically - provided that the text was written on a computer and the word-processing program kept track of the writing sessions. If images are available in digital form, it is also possible to automate the processing of image metadata, since in in such cases, image file formats, dimensions, color depths and resolutions are known. However, image <i>content</i> is meta-information that needs to be classified manually. Consequently, classification of metadata as manual or automatic must be performed on a case-by-case basis. Although the length of a text is meta-information, it can be determined automatically in such great detail that it transitions to the domain of automatically collectable metadata.
</p>
<a name="Statisticaldata"></a>
<h3>Statistical data</h3>

<p>
An essay is usually stored electronically in the form of alphanumerical character sequences, which computers can easily process. Consequently, data collection can be easily automated when the data pertains to the number of characters, words, sentences, sections and pages in an essay, or to the frequency of words and characters (all other frequencies generate insignificant results). However, this data has only limited significance for dicovering the content of the essay. Statistical analyses are much more useful for the comparison of several essays and their relation. Essay statistics can be compiled in order to compare between or among essays, to assess one essay against several others, or to analyze the entire essay collection or book.
</p>
<a name="Structuraldata"></a>
<h3>Structural data</h3>

<p>
Computer-linguistic processes can be used to evaluate grammatical and structural information in an essay. However, they require that the grammar of the language be well known and that the author of the text actually use it. A collection of purely associative words, even if they represent a statement of sorts, cannot be analyzed linguistically. And even if a linguistic analysis yields results, these results say little about the subject of the essay. They are better suited to understanding text genre or the complexity of a given author’s style.<span id="zu_2">
  <a href="#" onclick="Element.toggle( 'zu_2', 'detail_2' ); return false;">[ad.02]</a>.
</span><span id="detail_2" class="fussnote" style="display: none;">
  <br />
  And only rarely does this shed light on the complexity of the subject matter. Authors often write complex sentences about simple topics. Prose writers may purposely do so for reasons of style.
  <a href="#" onclick="Element.toggle( 'zu_2', 'detail_2' ); return false;">[schliessen]</a>
  <br />
</span>
</p>

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
/* 468x15, Erstellt 31.03.09, Variante 3 */
google_ad_slot = "4418230863";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

<a name="Dataselectioninthepresentcase"></a>
<h2>Data selection in the present case</h2>

<div class="textbild">
  <img src="02_originale/buurman_detail.jpg" class="bild">
  <div class="bildlegende">
    
      <a href="02_originale/buurman_detail_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
        Detail view graphic for Gerhard Buurman
  </div>
</div>
<p>
We have concentrated on data that was easy to collect automatically because otherwise too much subjective handwork would have been needed before graphics could be generated. The frequency of words both in an essay and in comparison to other essays provided us with a basis for graphics generation. In order to create detailed graphics with the greatest possible thematic significance we disregarded metadata and focused solely on word frequency. In order to remove from the statistics an essay’s non-thematic filler words (it, and, one, is, that, etc.), we used a manually compiled stop word list as a filter in the preliminary stage of the project. This influence is much smaller than manually signing keywords 
</p>
<p>
The identification of a "secondary author" was another vital criterion for the development of suitable visualizing graphics. The secondary author was identified for each examined word in an essay as the author who uses this special word most frequently in his/her essay amoung all other essays. Each examined word in the essay in question thus establishes a thematic link (at least in statistical terms) to another essays by referencing the essay in that the word has most importance beneath the one in question.
</p>
<a href="production.php">next page...</a>

<div style="clear: both;"></div>
<br /><br /><br /><br />

    </div>

<?php require( "_nav.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu en Navi
google_ad_channel = "5891472523";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="0229056246";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="6654244899";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="6654244899";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
