<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Graphical visualization of text similarities in essays in a book | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="en">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">

<a name="BProduction"></a>
<h1 style="margin-top: 1em;">B Production</h1>

<a name="Implementationcriteria"></a>
<h2>Implementation criteria</h2>

<p>
The graphics generated were intended for high-quality reproduction in the book. Since we wanted them to consist primarily of typography and colored areas, we selected a vector- rather than a pixel-based format. In addition, as an output file, the format was to be written by means of an algorithm to ensure a fully automated process. This made it essential to work with an ”open” format that had a well-known internal syntax and could be easily processed by book layout programs<span id="zu_3">
  <a href="#" onclick="Element.toggle( 'zu_3', 'detail_3' ); return false;">[ad.03]</a>.
</span><span id="detail_3" class="fussnote" style="display: none;">
  <br />
  We used Adobe Illustrator for graphics and Adobe InDesign for book layout.
  <a href="#" onclick="Element.toggle( 'zu_3', 'detail_3' ); return false;">[schliessen]</a>
  <br />
</span>The<span id="zu_4">
  <a href="#" onclick="Element.toggle( 'zu_4', 'detail_4' ); return false;">Scalable Vector Graphics format</a>
</span><span id="detail_4" class="fussnote" style="display: none;">
  <br />
  <b>Scalable Vector Graphics,</b> SVG. For more information, please visit <a href="http://www.w3.org/Graphics/SVG">http://www.w3.org/Graphics/SVG</a>
  <a href="#" onclick="Element.toggle( 'zu_4', 'detail_4' ); return false;">[schliessen]</a>
  <br />
</span>of the<span id="zu_5">
  <a href="#" onclick="Element.toggle( 'zu_5', 'detail_5' ); return false;">W3 Consortium</a>,
</span><span id="detail_5" class="fussnote" style="display: none;">
  <br />
  <b>World Wide Web Consortium</b>, W3C, W3C: World Wide Web Consortium, a group of industry professionals dedicated to standardizing electronic data formats. For more information, please visit <a href="http://www.w3.org">http://www.w3.org</a>
  <a href="#" onclick="Element.toggle( 'zu_5', 'detail_5' ); return false;">[schliessen]</a>
  <br />
</span>which was defined with the support of Adobe and other companies, fulfills these requirements. As an added advantage, it is readable by human beings in a form similar to<span id="zu_6">
  <a href="#" onclick="Element.toggle( 'zu_6', 'detail_6' ); return false;">XML</a>.
</span><span id="detail_6" class="fussnote" style="display: none;">
  <br />
  <b>XML:</b> eXtendable Markup Language, a meta-language used to describe data formats.
  <a href="#" onclick="Element.toggle( 'zu_6', 'detail_6' ); return false;">[schliessen]</a>
  <br />
</span>
</p>
<p>
An additional demand on the generation process was that it should permit quick and flexible alterations to the rules of graphics generation. This is why we selected Macromedia Director as an authoring tool with an interpreted programming language<span id="zu_7">
  <a href="#" onclick="Element.toggle( 'zu_7', 'detail_7' ); return false;">[ad.07]</a>
</span><span id="detail_7" class="fussnote" style="display: none;">
  <br />
  Director uses Lingo as programming language, whose syntax is closely related to PASCAL, a language developed for teaching purposes by Niklaus Wirth of the Swiss Federal Institute of Technology in Zurich.
  <a href="#" onclick="Element.toggle( 'zu_7', 'detail_7' ); return false;">[schliessen]</a>
  <br />
</span>in order to implement the algorithm. Macromedia Director allows for direct script editing without recompilation and features functions for manipulating texts, words and characters. Further, the authoring tool makes possible an uncomplicated display of graphics within the program in addition to output of the graphics as a text file in SVG format.
</p>

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
/* 468x15, Erstellt 31.03.09, Variante 3 */
google_ad_slot = "4418230863";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

<a name="Generationprocess"></a>
<h2>Generation process</h2>

<p>
The graphics generation process unfolds sequentially in a number of stages. It is partially repeated for each re-generation of the same graphic. It starts by reading and ”cleaning” the text – that is, by converting or deleting blank paragraphs, blank characters, special characters, etc. The text processed in this way is then filtered word by word (see ”stop word list” in "Data selection in the present case") and evaluated, yielding a table that contains all words that appear in the essay, plus their frequency. Data preparation only needs to be performed once per essay, and this information can then be stored as an intermediary result. At this point it is possible to compute how many words have a filler function, how many crop up several times and how many stand alone in the essay collection. Standardized values can be derived for the entire length of the essay.
</p>
<div class="textbild">
  <img src="03_fruehe_stadien/bisig_14.jpg" class="bild">
  <div class="bildlegende">
    
      <a href="03_fruehe_stadien/bisig_14_big.jpg" onclick="newWin=window.open( this.href, this.target, 'width=700, height=700, resizable=yes, toolBar=no, scrollbars=no' ); newWin.focus(); return false;" target="_blank" class="lupelink"><img src="px/lupe.gif" class="icon"></a>
        Early stages in the process to the final result
  </div>
</div>
<p>
In a second stage, correlations between the essay in question and other essays are determined by calculating the "secondary author". Both the "secondary author" and relative frequencies are calculated for every word in order to identify which of the two authors and which essay attaches "greatest importance" to the word.
</p>
<p>
This is followed by geometric computations that convert the captured data into graphic structures (in our case primarily circles). The position of the circles’ center, as well as radius length, filler colors, typeface color and circle transparency vary depending on data.
</p>
<p>
The graphic structures are expressed in the descriptive language SVG and written into an output file. Once all the descriptions for a content graphic have been pieced together, the file is closed and transferred to a browser program with a software component for the display of SVG files<span id="zu_8">
  <a href="#" onclick="Element.toggle( 'zu_8', 'detail_8' ); return false;">[ad.08]</a>.
</span><span id="detail_8" class="fussnote" style="display: none;">
  <br />
  In our case the Safari browser by Apple with the SVG plug-in by Adobe, available at <a href="http://www.adobe.com/svg/">http://www.adobe.com/svg/</a>
  <a href="#" onclick="Element.toggle( 'zu_8', 'detail_8' ); return false;">[schliessen]</a>
  <br />
</span>This component then converts the description into visual output.
</p>
<p>
The authoring tool Director also has a display function for vector-based graphics that permits direct display in the system and that would presumably have simplified the developmental process through simpler manipulation during graphics generation. However, we decided to forego this alternative output for reasons of time.
</p>

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
/* 468x15, Erstellt 31.03.09, Variante 3 */
google_ad_slot = "4418230863";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

<a name="Graphiclevels"></a>
<h2>Graphic levels</h2>

<p>
Our content graphics consist of three levels, each of which represents different information.
</p>
<a name="Background"></a>
<h3>Background</h3>

<div class="textbild">
  <img src="01_grafiken/konzentr_kreise.jpg" class="bild">
  <div class="bildlegende">
        Radius of concentric circles in relation to word count
  </div>
</div>
<p>
The background of each graphic classifies the essay in question within the context of all other essays in the book.
</p>
<p>
The concentric circles represent the individual essays, with the radius in direct proportion to the essay’s character count.
</p>
<div class="textbild">
  <img src="01_grafiken/kreis_sektoren.jpg" class="bild">
  <div class="bildlegende">
        Angle and radius of colored circle parts in relation to count and similarity of words in the essay
  </div>
</div>
<p>
In addition, the essays are depicted as a pie slice that has been color-coded according to <a href="http://de.wikipedia.org/wiki/Johannes_Itten" target="_blank">Johannes Itten's</a> color circle and whose width of the central angle derives from the number of coinciding words between the essay in question and that of the secondary author.
</p>
<p>
The radius of the pie slice also derives from the length of the individual essays (the same is true of the concentric circles) (pic.02). In the context of the book, the essays have been arranged so that the first essay appears at the top of the circle and the others follow in a clockwise direction.
</p>
<p>
The radius of the white space in the middle of the pie slices is relative to the length of the other essays (see pic.01). Both the distance between the center of the white space and its radius and the distance between its radius and the outermost concentric circle stand in direct proportion to the relationship between the length of the essay in question and that of the longest essay in the book. A short essay is thus represented by a small amount of central white space, a long essay by a large amount. The slices thus have ”lengths” corresponding to each of the essays. To illustrate the color code, we have given each slice a solid-colored outer edge that runs along the concentric circle allotted to the corresponding essay.
</p>
<a name="Barcodestructure"></a>
<h3>Barcode structure</h3>

<div class="textbild">
  <img src="01_grafiken/barcode.jpg" class="bild">
  <div class="bildlegende">
        Angle and color of barcode structure in relation to structure of the essay
  </div>
</div>
<p>
The barcode structure which appears within the concentric circle of the essay in question breaks down its structure.
</p>
<p>
Both the central angle and transparency of each bar correspond to the length of the essay’s paragraphs. A long paragraph in the text is depicted by a light shade (i.e. a high degree of transparency) and a wide central angle. The white space at the end of the barcode ring stands for the amount of source information and footnotes at the end of a text<span id="zu_9">
  <a href="#" onclick="Element.toggle( 'zu_9', 'detail_9' ); return false;">[ad.09]</a>.
</span><span id="detail_9" class="fussnote" style="display: none;">
  <br />
  This is the only manually identified metadata in the graphic.
  <a href="#" onclick="Element.toggle( 'zu_9', 'detail_9' ); return false;">[schliessen]</a>
  <br />
</span>A large amount of white space in the barcode ring indicates an essay with a lot of footnotes and source information.
</p>
<div style="clear: both;"></div>
<a name="Wordcircles"></a>
<h3>Word circles</h3>

<p>
The so-called word circles appear in the foreground of the graphic and represent a selection of the most frequently used words in the essay (pic.04). A word's frequency (and, to some extent, its importance) is proportional to both its type size and the diameter of its circle. The word circles' color is the basic color assigned to the author according to the color circle and the essay's position in the book. We have used a transparent shade to permit highlights and accents when different word circles overlap.
</p>
<div class="textbild">
  <img src="01_grafiken/wort_kreise.jpg" class="bild">
  <div class="bildlegende">
        Radius and position of word circles show importance of words for the essay in question and related essays
  </div>
</div>
<p>
On the one hand, the word circles are placed in different pie slices to indicate which secondary author used the word most frequently. On the other, their position on the radial axis between the author in question (in the center) and the secondary author (on the edge) reflects the frequency ratio between both essays: if the word is more "important" to the secondary author than to the author of the essay in question, the word circle appears closer to the secondary author - and vice versa.
</p>
<p>
Word circles in the center of the graphic are unallied and have no secondary author. The type color is gray and their radial position in the white space reflects their frequency in the text. Frequently appearing words are positioned in the center, seldom used words along the edge. The word circles’ central angles have been determined according to the position of the word’s first letter in the alphabet. In each pie slice, words beginning with a, b, c, come first and the rest run clockwise.
</p>

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
/* 468x15, Erstellt 31.03.09, Variante 3 */
google_ad_slot = "4418230863";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

<a name="Summary"></a>
<h2>Summary</h2>

<p>
Our goal was to create content graphics for the essays using a process marked by a high degree of automation and based on word frequency. In addition to content, we paid close attention to the (subjective) aesthetics of representation. In the developmental process described, we repeatedly considered the reader’s subjective impression of the graphics, fine-tuning the rules used to convert collected data into graphical structures. Our objective was to allow readers intuitively to form thematic conclusions about the essay from the informational graphics.
</p>

<a href="authors.php">next page...</a>

<br /><br /><br /><br />

    </div>

<?php require( "_nav.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu en Navi
google_ad_channel = "5891472523";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="0229056246";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="6654244899";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br><br><br>
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_alternate_color = "446688";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="6654244899";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
