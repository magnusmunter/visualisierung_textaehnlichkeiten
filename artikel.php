<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Grafische Visualisierung von Textähnlichkeiten zwischen Fach-Artikeln | munterbund.de</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Language" content="de">
  <meta name="copyright" content="(c) Copyright 2006 Magnus Rembold">
  <script src="../javascripts/prototype.js" type="text/javascript"></script>
  <script src="../javascripts/effects.js" type="text/javascript"></script>
  <script src="../javascripts/dragdrop.js" type="text/javascript"></script>
  <script src="../javascripts/controls.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="artikel.css" media="screen" />
</head>

<body>

  <div id="container">

    <div id="center" class="column">
      
<div class="grossbild"><img src="02_originale/fritz_gross_460.jpg"></div>
<a href="beispiele.php">...weitere Bildbeispiele</a>

<a name="Einleitung"></a>
<h2>Einleitung</h2>

<div class="textbild">
  <img src="px/totalinteraction.jpg" class="bild" style="width: 140px;">
</div>
<p>
Am <a href="http://iad.hgkz.ch" target="_blank">Studienschwerpunkt Interaction Design</a> der Hochschule für Gestaltung und Kunst Zürich wurde im Jahr 2004 ein Buch publiziert, das in seinen 19 enthaltenen Artikeln unterschiedliche Sichtweisen auf "die Interaktion" als Feld der Mensch-Maschine-Kommunikation aufzeigt. Dieses Buch sollte exemplarisch darstellen, in welch komplexem Themengebiet sich der Studiengang bewegt und ihn in Lehre und Forschung versucht, auszuloten.
</p>
<p>
Während der Entstehungszeit dieses Buchs <a href="http://iad.hgkz.ch/no_cache/de/projekte/projekt/total_interaction/" target="_blank">"Total Interaction"</a> entstand der Wunsch, die unterschiedlichen Artikel in ihrem losen oder festen thematischen Zusammenhang in einer geeigneten Weise zu visualisieren und grafisch darzustellen. Der Fokus lag dabei auf einer intuitiven visuellen Verständlichkeit der Visualisierungen, gleichwohl uns vollkommen bewusst war, dass der uninformierte Leser bestimmt nur einen kleinen Teil der enthaltenen Information in den Visualisierungen dechiffrieren kann.
</p>

<a href="konzept.php">nächste Seite...</a>
<br/><br/><br/><br/>

    </div>

<?php require( "_nav_de.html" ); ?>

    <div id="right" class="column">

<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 90;
google_ad_format = "160x90_0ads_al_s";
//2007-02-08: M Visu de Navi
google_ad_channel = "0551351158";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br /><br />
<script type="text/javascript"><!--
google_ad_client = "pub-5287177323997615";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
google_ad_channel ="3544438418";
google_color_border = "446688";
google_color_bg = "446688";
google_color_link = "EEEEEE";
google_color_text = "AACCEE";
google_color_url = "AACCEE";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
    </div>

  </div>

  <div id="footer"></div>

<div id="inhalt">

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1029998-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
